import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: "root"
})
export class AdminGuard implements CanActivate {

    constructor(
        private _authService: AuthService,
        private router: Router
    ) {

    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // Guard for user is login or not
        // let user = JSON.parse(localStorage.getItem('user'));
        // if (!user) {
        //     this.router.navigate(['/login']);
        //     return true;
        // }
        // return true;
        if (this._authService.isAuthenticated()) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}