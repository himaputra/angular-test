import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenModel } from '../model/token.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private currentLogin = 'currentUser';
    // jwtHelperService: JwtHelperService = new JwtHelperService();

    constructor(private httpClient: HttpClient) {}

    isAuthenticated(): boolean {
        return localStorage.getItem(this.currentLogin) != null;
    }

    login(user, pass) {
        let body = {
            username: user,
            password: pass
        }
        return this.httpClient.post<TokenModel>(`http://18.139.50.74:8080/login`, body, {observe: 'response'});
    }

    setToken(token): void {
        localStorage.setItem(this.currentLogin, token);
    }
}
