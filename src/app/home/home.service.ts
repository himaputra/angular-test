import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HomeService {

    constructor(private http: HttpClient) {}

    getList() {
        return this.http.get(`http://18.139.50.74:8080/checklist`, {observe: 'response'});
    }

}