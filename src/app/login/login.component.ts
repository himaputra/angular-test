import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../guard/auth.service';
import { TokenModel } from '../model/token.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted: false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      "username": this.formBuilder.control(""),
      "password": this.formBuilder.control("")
    });
  }

  login() {
    // console.log();
    let user = this.loginForm.value.username;
    let pass = this.loginForm.value.password;
    this.authService.login(user, pass).subscribe(resp => {
      console.log(resp.body.data);
      if (resp.status === 200) {
        console.log('True');
        this.authService.setToken(resp.body.data.token);
        this.router.navigateByUrl('/home');
      }
      // this.authService.setToken(resp)
    });
  }

}
