export class TokenModel {
    data: TokenDataModel;
}

export class TokenDataModel {
    token: string;
}